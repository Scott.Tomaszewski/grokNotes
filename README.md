# Grok Notes

See the [gitlab page for Grok Notes](http://scott.tomaszewski.gitlab.io/grokNotes/) 
and [Google Play Store alpha listing](https://play.google.com/apps/testing/com.grok.notes).  You can reach me at scottTomaszewski@gmail.com

I was unsatisfied with the markdown editors available on the Android platform.  I like to take notes on various topics and services like evernote are difficult to export from.  Therefore, I wrote my own!


## Features

- Commonmark (markdown) editor and preview
- Cloud sync via SAF (Google drive, Box, Internal storage, etc)
- Document operations: browse, create, edit, move
- Dual-pane view for tablets
- Formatting bar for quick insertions (see below)
- Find/replace in preview
- Customizations
    - Dark theme
    - Light theme primary and accent colors
    - Editor and preview font + text scale
- Configurations
    - Auto-save period
    - Screen keep-alive
    - Open doc to editor or preview
- Auto-formatting of passages copied from YouVersion app and bible.com

### Formatting bar items

- Indent line
- Unindent line
- Header (`#`)
- Bulletted list - asterisk (`*`)
- Emphasis - italics, bold (`**`)
- Smart asterisk - italics, bold, bulletted list (`*S*`)
- Bulletted list - hyphen (`-`)
- Code blocks (``` ` ```)
- Emphasis - italics, bold (`__`)
- Underline (`++++`)
- Strikethrough (`~~`)
- Open parenthesis (`(`)
- Close parenthesis (`)`)
- Parenthesis (`()`)
- Image (`![]()`)
- Link (`[]()`)
- Journal entry

## Changelog

### Alpha 9.1

- Bug fix: Corrects crash from failed backups due to file name

### Alpha 9.0 - Customizations!

- Adds dark-theme customization
- Adds light-theme primary and accent color customization
- Adds editor font and text scale customizations
- Adds preview font and text scale customizations
- Bug fix: Adds missing borders to commonmark tables
- Bug fix: Adds background to about view to avoid view overlap

### Alpha 8.0

- Adds support for rendering images in the preview page
- Adds option for opening documents directly to the preview page
- Adds menu item for jumping to the end of a document in the editor
- Adds space to the bottom of the preview page for FAB
- Bug fix: Hides the vertical bar on the about page on tablets
- Bug fix: Unordered list formatting bar items correctly respect indents
- Bug fix: Avoids duplicate unordered list insertions

### Alpha 7.1

- Bug fix: Optimizes formatting bar items that affect line beginnings to run smoothly on large docs

### Alpha 7

- Adds an about page for version info and OSS links
- Adds simple welcome screen for aesthetics
- Automatically creates a backup of newly opened files (path in about page)
- Collapses the formatting bar if all items are disabled
- Bug fix: Avoids crashing from certain formatting bar items

### Alpha 6

- Major overhaul of the formatting bar in general
- Adds new formatting bar items including "smart" items
- Avoids showing "done" while in landscape mode
- Adjusts margins on large screens
- Bug fix: corrects status bar color in preference screen
- Bug fix: updates formatting bar items after preference change
- Bug fix: properly handles undo/redo when using various formatting bar items
- Bug fix: correctly shows (mini) formatting bar in landscape mode

### Alpha 5

- Adds two-pane support for tablets
- Adds underline support by surrounding content with `++`.  Ex: `++hi++` will underline "hi"
- Adds multi-line tab and reverse-tab support
- Adds zoom and scroll support to the preview view
- Bug fix: Corrects formatting bar link syntax from `()[]` to `[]()`
- Bug fix: Avoids crashing when bible verse fails to parse
- Bug fix: Failing to load document no longer crashes the app
- Bug fix: Correctly shows FAB after opening preferences and turning off screen
- Bug fix: Avoids transparent background on preferences
- Bug fix: Links open in browser when clicked from preview view
- Bug fix: Rich text formatting pasted into the editor is stripped to plaintext
- Bug fix: Correctly hides the FAB when loading content into the editor

### Alpha 4

- Adds undo/redo support within editor
- Adds setting for keeping the screen alive to avoid screen timeout
- Adds MIT licence - [MIT license](https://opensource.org/licenses/MIT)
- Adds preference for allowing the file browser to open any file mime type, not just `text/*`
- Adds "reverse tab" button for formatting bar
- Refactors tab button to affect the beginning of the line instead of cursor position
- Fixes bug causing a crash when backgrounding app while editor is loading
- Fixes bug causing back button issue after using preference screen
- Fixes bug causing a crash on startup after not using the app for a time

### Alpha 3

- Better error/timeout handling when app fails to load or save a document
- Removes the "File opened successfully." snackbar on load
- Corrects bug which prevented a second document from opening under various conditions
- Properly stops (and resets) periodic auto-save when interval preference changes and when new document is opened 
- Reduces noise in logs
- Correct bug that caused app to crash when edit view is loading a document and rotation occurs
- Disables actions within the edit view while the document is loading to avoid strange behavior

### Alpha 2

- Bumps commonmark to version 0.6.0.
- Corrects bug that caused crash on boot when loading SAF document while still in memory.
- Adds "Entry" button to formatting bar.  Entries consist of a horizontal-rule and the date.
- Adds support for converting copied passages from bible.com and YouVersion app into markdown
- GrokNotes now remembers the cursor location of notes even after app is closed.
- Removes all processors except commonmark
    - Other processors were non-standard or prevented features like find-in-document

## Releasing

There are a few more bugs to resolve and features to add, then I will open it up to the public app store.
For now you can [opt-in to the alpha release here on the play store](https://play.google.com/apps/testing/com.grok.notes).

Also, all the releases can be found on the [tags page](https://gitlab.com/Scott.Tomaszewski/grokNotes/tags)