package com.grok.notes;

import android.content.Context;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;
import static android.text.Selection.getSelectionEnd;
import static android.text.Selection.getSelectionStart;

public enum FormattingBarItem {
    REVERSE_TAB("Enable the |<- button. Applies at start of line.",
            "Reverse tab", "fb_tab_enabled", true,
            Icon.ofFlipped(R.drawable.ic_keyboard_tab_black_48dp),
            Op.removeTabAtStartOfLines()
    ),
    TAB("Enable the ->| button. Applies at start of line.",
            "Tab", "fb_reverse_tab_enabled", true,
            Icon.of(R.drawable.ic_keyboard_tab_black_48dp),
            Op.atStartOfLines(Strings.repeat(" ", Preference.TAB_LENGTH))
    ),
    OCTOTHORPE("Enable the # button. Applies at start of line.",
            "Octothorpe (Header)", "fb_octothorpe_enabled", true,
            Icon.of("#"),
            Op.atStartOfLines("#")
    ),
    ASTERISK("Enable the * button. Applies at first char in line.",
            "Asterisk (bulleted list)", "fb_asterisk_enabled", false,
            Icon.of("*"),
            Op.atFirstCharInLines("* ")
    ),
    EMPHASIS_ASTERISK("Enable the ** button. Wraps selected text.",
            "Surrounding asterisks (italics, bold)", "fb_emphasis_asterisk_enabled", false,
            Icon.of("**"),
            Op.surroundSelection("*", "*")
    ),
    SMART_ASTERISK("Enable the smart *S* button. Wraps selected text.",
            "Smart asterisks (italics, bold, bulleted list)", "fb_smart_asterisk_enabled", true,
            Icon.of("*S*"),
            Op.smartSurround("*")
    ),
    HYPHEN("Enable the - button. Replaces selected text.",
            "Hyphen (bulleted list)", "fb_hyphen_enabled", false,
            Icon.of("-"),
            Op.replaceSelection("- ")
    ),
    LIST_HYPHEN("Enable the - button. Applies at first char in line.",
            "List hyphen (bulleted list)", "fb_sol_hyphen_enabled", true,
            Icon.of("-"),
            Op.atFirstCharInLines("-")
    ),
    BACK_TICK("Enable the `` button. Wraps selected text",
            "Back tick (code block)", "fb_back_tick_enabled", false,
            Icon.of("``"),
            Op.surroundSelection("`", "`")
    ),
    UNDERSCORE("Enable the __ button",
            "Underscore (italics, bold)", "fb_underscore_enabled", false,
            Icon.of("__"),
            Op.surroundSelection("_", "_")
    ),
    UNDERLINE("Enable the ++++ button",
            "Underline", "fb_underline_enabled", true,
            Icon.of("++"),
            Op.surroundSelection("++", "++")
    ),
    STRIKETHROUGH("Enable the ~~ button",
            "Strikethrough", "fb_tilde_enabled", false,
            Icon.of("~~"),
            Op.surroundSelection("~~", "~~")
    ),
    LEFT_PARENTHESIS("Enable the ( button",
            "Left parenthesis", "fb_left_parenthesis_enabled", false,
            Icon.of("("),
            Op.replaceSelection("(")
    ),
    RIGHT_PARENTHESIS("Enable the ) button",
            "Right parenthesis", "fb_right_parenthesis_enabled", false,
            Icon.of(")"),
            Op.replaceSelection(")")
    ),
    PARENTHESIS("Enable the () button",
            "Parenthesis", "fb_parenthesis_enabled", false,
            Icon.of("()"),
            Op.surroundSelection("(", ")")
    ),
    IMAGE("Enable the ![]() button",
            "Image", "fb_image_enabled", false,
            Icon.of("![]()"),
            Op.surroundSelection("![", "]()")
    ),
    LINK("Enable the []() button",
            "Link", "fb_link_enabled", true,
            Icon.of("[]()"),
            Op.surroundSelection("[", "]()")
    ),
    ENTRY("Enable the entry button",
            "Entry", "fb_entry_enabled", true,
            Icon.of(R.drawable.ic_playlist_add_black_48dp),
            Op.replaceSelection("\n\n---\n\n" + DateFormat.getDateInstance().format(new Date()) + "\n\n")
    );

    public static boolean anyEnabled(Context c) {
        for (FormattingBarItem i : values()) {
            if (getDefaultSharedPreferences(c).getBoolean(i.key, i.enabledByDefault)) {
                return true;
            }
        }
        return false;
    }

    final String summary;
    final String title;
    final String key;
    final Icon icon;
    final Op steps;
    final boolean enabledByDefault;

    FormattingBarItem(String summary, String title, String key, boolean enabledByDefault, Icon icon, Op steps) {
        this.summary = summary;
        this.title = title;
        this.key = key;
        this.icon = icon;
        this.steps = steps;
        this.enabledByDefault = enabledByDefault;
    }

    public void applyTo(EditText source) {
        steps.applyTo(source);
    }

    public void applyOnClickOf(LinearLayout bar, final EditText source) {
        if (getDefaultSharedPreferences(source.getContext()).getBoolean(key, enabledByDefault)) {
            View button = icon.build(bar.getContext());
            bar.addView(button);
            button.setOnClickListener(v -> applyTo(source));
        }
    }

    private static abstract class Op {
        public static Op replaceSelection(String replacement) {
            return new Op() {
                @Override
                protected void applyTo(EditText source) {
                    source.getText().replace(
                            source.getSelectionStart(),
                            source.getSelectionEnd(),
                            replacement);
                }
            };
        }

        public static Op atFirstCharInLines(String addText) {
            return atStartOfLines(addText, true);
        }

        public static Op atStartOfLines(String addText) {
            return atStartOfLines(addText, false);
        }

        private static Op atStartOfLines(String addText, boolean ignoreLineWhitespace) {
            return new Op() {
                @Override
                protected void applyTo(EditText source) {
                    int s = source.getSelectionStart();
                    int e = source.getSelectionEnd();
                    List<Integer> charPosOfLines = findLineIndicesForSelection(
                            source.getText(), ignoreLineWhitespace);
                    int startPosition = Ints.min(Ints.toArray(charPosOfLines));
                    int endPosition = Ints.max(Ints.toArray(charPosOfLines));

                    Editable replacementContent = new SpannableStringBuilder(
                            source.getText().subSequence(startPosition, endPosition));
                    int offset = 0;
                    for (int rawLinePosition : charPosOfLines) {
                        int adjustedForSubstring = rawLinePosition - startPosition;
                        int adjustedForPreviousReplacements = adjustedForSubstring + offset;

                        replacementContent.replace(
                                adjustedForPreviousReplacements,
                                adjustedForPreviousReplacements,
                                addText);
                        offset += addText.length();
                    }
                    source.getText().replace(startPosition, endPosition, replacementContent);
                    source.setSelection(s + addText.length(), e + offset);
                }
            };
        }

        public static Op surroundSelection(String prefix, String suffix) {
            return new Op() {
                @Override
                protected void applyTo(EditText source) {
                    int s = source.getSelectionStart();
                    int e = source.getSelectionEnd();
                    CharSequence replacement = prefix + source.getText().subSequence(s, e) + suffix;
                    source.getText().replace(s, e, replacement);
                    source.setSelection(s + prefix.length(), e + prefix.length());
                }
            };
        }

        public static Op smartSurround(String toInsert) {
            return new Op() {
                @Override
                protected void applyTo(EditText source) {
                    if (source.getSelectionStart() == source.getSelectionEnd()) {
                        Op.replaceSelection(toInsert).applyTo(source);
                    } else {
                        Op.surroundSelection(toInsert, toInsert).applyTo(source);
                    }
                }
            };
        }

        public static Op removeTabAtStartOfLines() {
            return new Op() {
                @Override
                protected void applyTo(EditText source) {
                    String tab = Strings.repeat(" ", Preference.TAB_LENGTH);
                    List<Integer> charPosOfLines = findLineIndicesForSelection(source.getText(), false);
                    int startPosition = Ints.min(Ints.toArray(charPosOfLines));
                    int endPosition = Ints.max(Ints.toArray(charPosOfLines)) + tab.length();

                    if (endPosition > source.length()) {
                        return;
                    }

                    Editable replacementContent = new SpannableStringBuilder(
                            source.getText().subSequence(startPosition, endPosition));
                    int offset = 0;
                    for (int rawLinePosition : charPosOfLines) {
                        int adjustedForSubstring = rawLinePosition - startPosition;
                        int adjustedForPreviousReplacements = adjustedForSubstring + offset;
                        String maybeTab = replacementContent.subSequence(adjustedForPreviousReplacements,
                                adjustedForPreviousReplacements + tab.length()).toString();
                        if (maybeTab.equals(tab)) {
                            replacementContent.replace(
                                    adjustedForPreviousReplacements,
                                    adjustedForPreviousReplacements + tab.length(),
                                    "");
                            offset -= tab.length();
                        }
                    }
                    source.getText().replace(startPosition, endPosition, replacementContent);
                }
            };
        }

        protected abstract void applyTo(EditText source);
    }

    /**
     * Returns the line indices contained in the selection and the
     */
    private static List<Integer> findLineIndicesForSelection(
            Spannable text, boolean ignoreLineWhitespace) {
        return findLineIndicesForSelection(
                text.toString(),
                getSelectionStart(text),
                getSelectionEnd(text),
                ignoreLineWhitespace);
    }

    @VisibleForTesting
    static List<Integer> findLineIndicesForSelection(
            String text, int selectionStart, int selectionEnd, boolean ignoreLineWhitespace) {
        List<Integer> lineIndices = new ArrayList<>();
        lineIndices.add(lineIndexBeforeSelection(text, selectionStart, ignoreLineWhitespace));
        lineIndices.addAll(lineIndicesWithinSelection(text, selectionStart, selectionEnd, ignoreLineWhitespace));
        return lineIndices;
    }

    @VisibleForTesting
    static List<Integer> lineIndicesWithinSelection(
            String text, int selectionStart, int selectionEnd, boolean ignoreLineWhitespace) {
        List<Integer> indices = new ArrayList<>();
        for (int i = selectionStart; i <= selectionEnd; i++) {
            // dont look before the start
            int withinRange = Math.max(selectionStart, i - 2);
            if (isNewline(text.subSequence(withinRange, i).toString())) {
                indices.add(ignoreLineWhitespace ? indexOfNextNonWhitespaceChar(text, i) : i);
            }
        }
        return indices;
    }

    @VisibleForTesting
    static int indexOfNextNonWhitespaceChar(String text, int start) {
        for (int j = start; j < text.length(); j++) {
            if (text.charAt(j) != ' ' && text.charAt(j) != '\t') {
                return j;
            }
        }
        return text.length();
    }

    @VisibleForTesting
    static int lineIndexBeforeSelection(String text, int selectionStart, boolean ignoreLineWhitespace) {
        for (int i = selectionStart - 1; i >= 0; i--) {
            if (isNewline("" + text.charAt(i))) {
                return ignoreLineWhitespace ? indexOfNextNonWhitespaceChar(text, i + 1) : i + 1;
            }
        }
        // if newline doesnt exist, then we've reached beginning of the file
        return ignoreLineWhitespace ? indexOfNextNonWhitespaceChar(text, 0) : 0;
    }

    private static boolean isNewline(String toCheck) {
        return toCheck.endsWith("\n") || toCheck.endsWith("\r")
                || toCheck.endsWith("\r\n") || toCheck.endsWith("\n\r");
    }

    private static abstract class Icon {
        public static Icon of(String iconText) {
            return new Icon() {
                @Override
                public View build(Context c) {
                    TextView button = new TextView(c);
                    button.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            1));
                    button.setText(iconText);
                    button.setGravity(Gravity.CENTER);
                    button.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                            c.getResources().getDimension(R.dimen.formatting_bar_text_size));
                    button.setTextColor(c.getResources().getColor(android.R.color.primary_text_light));
                    return button;
                }
            };
        }

        public static Icon ofFlipped(int imageResToFlip) {
            return new Icon() {
                @Override
                public View build(Context c) {
                    View v = Icon.of(imageResToFlip).build(c);
                    v.setScaleX(-1);
                    return v;
                }
            };
        }

        public static Icon of(int imageResource) {
            return new Icon() {
                @Override
                public View build(Context c) {
                    ImageView image = new ImageView(c);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                            Math.round(c.getResources().getDimension(R.dimen.formatting_bar_text_size)),
                            ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.gravity = Gravity.CENTER;
                    image.setLayoutParams(layoutParams);
                    image.setImageResource(imageResource);

                    FrameLayout frame = new FrameLayout(c);
                    frame.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            1));
                    frame.addView(image);
                    return frame;
                }
            };
        }

        public abstract View build(Context c);
    }
}
