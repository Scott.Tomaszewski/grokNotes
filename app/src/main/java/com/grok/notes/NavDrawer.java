package com.grok.notes;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.grok.notes.document.StorageAccessFramework;

public abstract class NavDrawer {
    public static NavDrawer create(final Activity context, Toolbar bar) {
        DrawerLayout drawer = (DrawerLayout) context.findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                context, drawer, bar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                Keyboard.close(context, null);
                super.onDrawerOpened(drawerView);
            }
        };
        toggle.setToolbarNavigationClickListener(v -> context.onBackPressed());
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) context.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(Clicks.create(context, toggle));
        return new NavDrawer() {
            @Override
            public void setDrawerIndicatorEnabled(boolean enabled) {
                toggle.setDrawerIndicatorEnabled(enabled);
            }
        };
    }

    public abstract void setDrawerIndicatorEnabled(boolean enabled);

    private static abstract class Clicks implements NavigationView.OnNavigationItemSelectedListener {
        private static Clicks create(final Activity context, final ActionBarDrawerToggle toggle) {
            return new Clicks() {
                @SuppressWarnings("StatementWithEmptyBody")
                @Override
                public boolean onNavigationItemSelected(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.saf_documents:
                            StorageAccessFramework.openMarkdownDocument(context);
                            break;
                        case R.id.create_doc:
                            StorageAccessFramework.createNewMarkdownDocument(context, "grokNote.md");
                            break;
                        case R.id.preferences:
                            openSettings(context, toggle);
                            break;
                        case R.id.about:
                            openAbout(context, toggle);
                            break;
                    }
                    DrawerLayout drawer = (DrawerLayout) context.findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                    return true;
                }
            };
        }
    }

    private static void openSettings(Activity context, ActionBarDrawerToggle toggle) {
        toggle.setDrawerIndicatorEnabled(false);
        context.getFragmentManager().beginTransaction()
                .replace(R.id.welcome, Blank.FRAGMENT)
                .replace(R.id.contentContainer, new SettingsFragment())
                .addToBackStack(null)
                .commit();
    }

    private static void openAbout(Activity context, ActionBarDrawerToggle toggle) {
        toggle.setDrawerIndicatorEnabled(false);
        context.getFragmentManager().beginTransaction()
                .replace(R.id.welcome, Blank.FRAGMENT)
                .replace(R.id.contentContainer, new AboutFragment())
                .addToBackStack(null)
                .commit();
    }
}
