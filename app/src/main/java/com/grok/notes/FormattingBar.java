package com.grok.notes;

import android.app.Activity;
import android.support.design.widget.CoordinatorLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

public class FormattingBar {
    public static FormattingBar enable(Activity context) {
        moveFabToHeight((int) context.getResources().getDimension(R.dimen.formatting_bar_height), context);
        View bar = context.findViewById(R.id.formattingBar);
        if (FormattingBarItem.anyEnabled(context)) {
            bar.setVisibility(View.VISIBLE);
        }
        return new FormattingBar(context, bar);
    }

    public static void disable(Activity context) {
        View v = context.findViewById(R.id.formattingBar);
        v.setVisibility(View.GONE);
        moveFabToHeight(0, context);
    }

    private static void moveFabToHeight(int offsetFromDefault, Activity context) {
        CoordinatorLayout.LayoutParams lp = new CoordinatorLayout.LayoutParams(
                CoordinatorLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        int margin = (int) context.getResources().getDimension(R.dimen.fab_margin);
        lp.setMargins(margin, margin, margin, margin + offsetFromDefault);
        lp.gravity = Gravity.BOTTOM | Gravity.END;
        context.findViewById(R.id.fab).setLayoutParams(lp);
    }

    private final Activity context;
    private final View bar;

    public FormattingBar(Activity context, View bar) {
        this.context = context;
        this.bar = bar;
    }

    public FormattingBar applyTo(EditText input) {
        LinearLayout fb = (LinearLayout) bar;
        fb.removeAllViews();
        for (FormattingBarItem i : FormattingBarItem.values()) {
            i.applyOnClickOf(fb, input);
        }
        addKeyboardListener(context);
        return this;
    }

    public FormattingBar hide() {
        disable(context);
        return this;
    }

    private void addKeyboardListener(final Activity context) {
        Keyboard.add(context, new Keyboard.OnKeyboardVisibilityListener() {
            @Override
            public void onVisibilityChanged(boolean visible) {
                if (visible) {
                    FormattingBar.enable(context);
                } else {
                    FormattingBar.disable(context);
                }
            }
        });
    }
}
