package com.grok.notes.edit;

import android.app.Activity;
import android.view.View;

import com.google.common.base.Optional;
import com.grok.notes.R;

public class Divider {
    public static void hideIfPresent(Activity a) {
        Optional<View> divider = get(a);
        if (divider.isPresent()) {
            divider.get().setVisibility(View.GONE);
        }
    }

    public static void show(Activity a) {
        Optional<View> divider = get(a);
        if (divider.isPresent()) {
            divider.get().setVisibility(View.VISIBLE);
        }
    }

    public static Divider saveState(Activity a) {
        Optional<View> divider = get(a);
        return new Divider(divider.isPresent()
                ? Optional.of(divider.get().getVisibility())
                : Optional.absent());
    }

    private static Optional<View> get(Activity a) {
        return Optional.fromNullable(a.findViewById(R.id.divider));
    }

    private final Optional<Integer> visibility;

    private Divider(Optional<Integer> visibility) {
        this.visibility = visibility;
    }

    public void restore(Activity a) {
        Optional<View> divider = get(a);
        if (divider.isPresent() && visibility.isPresent()) {
            //noinspection WrongConstant
            divider.get().setVisibility(visibility.get());
        }
    }
}
