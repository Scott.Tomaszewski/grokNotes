package com.grok.notes.edit;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

import com.google.common.base.Optional;

public class CursorAwareEditText extends EditText {
    private Optional<OnCursorChangeListener> callback = Optional.absent();

    public CursorAwareEditText(Context context) {
        super(context);
    }

    public CursorAwareEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CursorAwareEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CursorAwareEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        if (callback == null) {
            Log.d("grok", "By some sorcery my variable is null");
            callback = Optional.absent();
        }
        if (callback.isPresent()) {
            callback.get().onCursorChange(selStart, selEnd);
        }
        super.onSelectionChanged(selStart, selEnd);
    }

    public void setCursorChangeListener(OnCursorChangeListener callback) {
        this.callback = Optional.fromNullable(callback);
        Log.d("grok", "setChangeListener");
    }

    public interface OnCursorChangeListener {
        void onCursorChange(int selStart, int selEnd);
    }
}
