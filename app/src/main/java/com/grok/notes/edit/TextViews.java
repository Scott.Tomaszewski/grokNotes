package com.grok.notes.edit;

import android.widget.EditText;

public class TextViews {
    public static void insertAtCursor(EditText destination, String toInsert, int postInsertCursorOffset) {
        int location = destination.getSelectionStart();
        String text = destination.getText().toString();
        destination.setText(""
                + text.subSequence(0, location)
                + toInsert
                + text.subSequence(location, text.length()));
        destination.setSelection(location + postInsertCursorOffset);
    }

    private TextViews() {
    }
}
