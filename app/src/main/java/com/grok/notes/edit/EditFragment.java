package com.grok.notes.edit;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentResolver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.common.base.Optional;
import com.grok.notes.FormattingBar;
import com.grok.notes.Keyboard;
import com.grok.notes.Preference;
import com.grok.notes.ProgressBar;
import com.grok.notes.R;
import com.grok.notes.WelcomeFragment;
import com.grok.notes.bible.Passage;
import com.grok.notes.document.SafDocument;
import com.grok.notes.preview.PreviewFragment;
import com.grok.notes.util.Activities;
import com.grok.notes.util.AsyncTaskWithTimeout;
import com.grok.notes.util.FAB;
import com.grok.notes.util.Font;
import com.grok.notes.util.PeriodicAsyncTask;
import com.grok.notes.util.ScrollPosition;
import com.grok.notes.util.UndoRedoHelper;

import java.io.IOException;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

public class EditFragment extends Fragment {
    private static final String ARG_CONTENT = "content";
    private static final String ARG_CURSOR = "cursor";

    private SafDocument content;
    private ScrollView scroll;
    private ScrollPosition cursor;
    private CutCopyPasteEditText edit;
    private String latestSaved;
    private String latestPreview = "";
    private boolean contentIsPopulated = false;
    private Timer periodicSave;
    private Timer periodicPreview;
    int originalInputType;
    private ViewTreeObserver.OnGlobalLayoutListener fabVisibilityListener;
    private UndoRedoHelper history;

    public static EditFragment of(SafDocument from, ScrollPosition cursor) {
        EditFragment fragment = new EditFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CONTENT, from);
        args.putParcelable(ARG_CURSOR, cursor);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            content = getArguments().getParcelable(ARG_CONTENT);
            cursor = getArguments().getParcelable(ARG_CURSOR);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Font.editorPreference(getActivity()).applyTo((ViewGroup) this.getView());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit, menu);
        Optional<ContentResolver> c = Activities.contentResolver(getActivity());
        if (c.isPresent()) {
            getActivity().setTitle(content.filename(c.get()));
        } else {
            Log.w("grok", "Failed to load title: missing ContentResolver");
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (getActivity().findViewById(R.id.leftContainer).getVisibility() == View.VISIBLE) {
            getActivity().getMenuInflater().inflate(R.menu.edit, menu);
        }
        if (canDualPane()) {
            if (getActivity().findViewById(R.id.rightContainer).getVisibility() == View.VISIBLE) {
                getActivity().getMenuInflater().inflate(R.menu.preview, menu);
            }
            menu.add(0, R.id.dualMode, 140, "Dual pane")
                    .setIcon(R.drawable.ic_chrome_reader_mode_white_48dp)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }
        if (isDualPane()) {
            menu.findItem(R.id.edit).setVisible(false);
            menu.findItem(R.id.preview).setVisible(false);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!contentIsPopulated) {
            Toast.makeText(getActivity(), "Cannot take actions until content loads.", Toast.LENGTH_SHORT).show();
            return true;
        }
        switch (item.getItemId()) {
            case R.id.undo:
                history.undo();
                return true;
            case R.id.redo:
                history.redo();
                return true;
            case R.id.preview:
                if (canDualPane()) {
                    previewOnlyFromDual(FAB.orNull(getActivity()));
                } else {
                    previewWhenAble();
                }
                return true;
            case R.id.save:
                if (!save()) {
                    Snackbar.make(edit, "Failed to save document!  Try again.", Snackbar.LENGTH_LONG).show();
                }
                return true;
            case R.id.dualMode:
                toggleDualMode(FAB.orNull(getActivity()));
                return true;
            case R.id.renameMove:
                moveDocument();
                return true;
            case R.id.jumpToEnd:
                scrollToEnd();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        ProgressBar.hide(getActivity());
        FormattingBar.disable(getActivity());
        Keyboard.ignoreFabVisibilityListener(this.getActivity(), fabVisibilityListener);
        Keyboard.close(getActivity(), edit);
        if (periodicSave != null) {
            periodicSave.cancel();
        }
        if (periodicPreview != null) {
            periodicPreview.cancel();
        }
        save();
        contentIsPopulated = false;
        edit.getText().clear();
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater i, ViewGroup container, Bundle savedInstanceState) {
        View root = i.inflate(R.layout.fragment_edit, container, false);
        edit = (CutCopyPasteEditText) root.findViewById(R.id.edit);
        edit.setOnCutCopyPasteListener(new CutCopyPasteEditText.Listener() {
            @Override
            public boolean onPaste(String contentsToPaste) {
                Optional<Passage> maybe = Passage.parse(contentsToPaste);
                if (maybe.isPresent()) {
                    // handle passages
                    String formatted = maybe.get().asMarkdown();
                    TextViews.insertAtCursor(edit, formatted, formatted.length());
                    return true;
                }
                // override formatted text paste - use string raw instead.
                TextViews.insertAtCursor(edit, contentsToPaste, contentsToPaste.length());
                return true;
            }
        });
        scroll = (ScrollView) root.findViewById(R.id.scroll);
        originalInputType = edit.getInputType();
        edit.setInputType(EditorInfo.TYPE_NULL);
        edit.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        float scaledTextSize = edit.getTextSize() / edit.getPaint().density;
        edit.setTextSize(scaledTextSize + Preference.EDITOR_TEXT_SIZE.getInt(getActivity(), 0));
        contentIsPopulated = false;
        final FloatingActionButton fab = FAB.setGone(getActivity()).get();
        FAB.setImage(getActivity(), R.drawable.ic_visibility_white_48dp);
        if (isDualPane()) {
            fab.setOnClickListener(v -> previewOnlyFromDual(fab));
        } else {
            fab.setOnClickListener(openPreviewWhenAble(fab));
        }
        Divider.show(getActivity());
        return root;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        edit.getText().clear();
        ProgressBar.show(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        FormattingBar.enable(getActivity()).applyTo(edit).hide();
        this.fabVisibilityListener = Keyboard.hideFabWhenOpen(getActivity());
        enablePeriodicSave();
        populateText(edit);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ARG_CONTENT, content);
        outState.putParcelable(ARG_CURSOR, contentIsPopulated ? ScrollPosition.from(scroll, edit) : cursor);
    }

    private void previewOnlyFromDual(final FloatingActionButton fab) {
        getActivity().findViewById(R.id.leftContainer).setVisibility(View.GONE);
        getActivity().findViewById(R.id.rightContainer).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.divider).setVisibility(View.GONE);
        updatePreview();
        FAB.setImage(getActivity(), R.drawable.ic_mode_edit_white_48dp);
        fab.setOnClickListener(v -> dualPane(fab));
        getActivity().invalidateOptionsMenu();
    }

    private void previewOnlyFromEditor(final FloatingActionButton fab) {
        getActivity().findViewById(R.id.leftContainer).setVisibility(View.GONE);
        getActivity().findViewById(R.id.rightContainer).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.divider).setVisibility(View.GONE);
        updatePreview();
        FAB.setImage(getActivity(), R.drawable.ic_mode_edit_white_48dp);
        fab.setOnClickListener(v -> editorOnly(fab));
        getActivity().invalidateOptionsMenu();
    }

    // Visible for previewFragment
    public void editorOnly(final FloatingActionButton fab) {
        getActivity().findViewById(R.id.leftContainer).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.rightContainer).setVisibility(View.GONE);
        getActivity().findViewById(R.id.divider).setVisibility(View.GONE);
        FAB.setImage(getActivity(), R.drawable.ic_visibility_white_48dp);
        fab.setOnClickListener(v -> previewOnlyFromEditor(fab));
        getActivity().invalidateOptionsMenu();
    }

    private void dualPane(final FloatingActionButton fab) {
        getActivity().findViewById(R.id.leftContainer).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.rightContainer).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.divider).setVisibility(View.VISIBLE);
        FAB.setImage(getActivity(), R.drawable.ic_visibility_white_48dp);
        fab.setOnClickListener(v -> previewOnlyFromDual(fab));
        getActivity().invalidateOptionsMenu();
    }

    private void toggleDualMode(FloatingActionButton fab) {
        View preview = getActivity().findViewById(R.id.rightContainer);
        View editor = getActivity().findViewById(R.id.leftContainer);
        if (editor.getVisibility() == View.GONE || preview.getVisibility() == View.GONE) {
            dualPane(fab);
        } else if (editor.getVisibility() == View.VISIBLE && preview.getVisibility() == View.VISIBLE) {
            editorOnly(fab);
        }
    }

    @NonNull
    private View.OnClickListener openPreviewWhenAble(final FloatingActionButton fab) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!contentIsPopulated) {
                    Toast.makeText(getActivity(), "Cannot take actions until content loads.", Toast.LENGTH_SHORT).show();
                    return;
                }
                ProgressBar.show(getActivity());
                // prevent double clicks
                FAB.doNothingOnClick(getActivity());
                previewWhenAble();
            }
        };
    }

    private void moveDocument() {
        try {
            content.move(getActivity());
        } catch (IOException e) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Failed to move document")
                    .setMessage("Could not move document.  Please ensure that you " +
                            "are connected to the internet and try again.")
                    .setPositiveButton("Back", WelcomeFragment.showFromDialog(getActivity()))
                    .show();
        }
    }

    private void scrollToEnd() {
        scroll.post(() -> scroll.fullScroll(ScrollView.FOCUS_DOWN));
    }

    private void previewWhenAble() {
        // keep trying to preview until successfully saved
        if (!previewIfAble()) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    while (!previewIfAble() && !isCancelled()) {
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            Log.w("grok", "OpenPreview interrupted");
                        }
                    }
                    return null;
                }
            }.execute();
        }
    }

    private boolean previewIfAble() {
        if (save()) {
            String md = edit.getText().toString();
            PreviewFragment f = PreviewFragment.create(md, content, ScrollPosition.from(scroll, edit));
            getActivity().getFragmentManager().beginTransaction().replace(R.id.leftContainer, f).commit();
            Snackbar.make(edit, "File saved successfully.", Snackbar.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    private boolean canDualPane() {
        return getActivity().findViewById(R.id.rightContainer) != null;
    }

    private boolean isDualPane() {
        Activity a = getActivity();
        if (a != null) {
            View left = a.findViewById(R.id.leftContainer);
            View right = a.findViewById(R.id.rightContainer);
            return left != null && left.getVisibility() == View.VISIBLE
                    && right != null && right.getVisibility() == View.VISIBLE;
        }
        return false;
    }

    private void populateText(final EditText edit) {
        new AsyncTaskWithTimeout<Void, Void, String>(getActivity(), 10, TimeUnit.SECONDS) {
            @Override
            protected String runInBackground(Void... params) {
                ProgressBar.show(getActivity());
                Optional<ContentResolver> c = Activities.contentResolver(getActivity());
                if (c.isPresent()) {
                    try {
                        return content.read(c.get());
                    } catch (IOException failed) {
                        return null;
                    }
                } else {
                    Log.w("grok", "Failed to load document: missing ContentResolver");
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String s) {
                if (getActivity() == null) return;
                if (s != null && edit != null) {
                    edit.getText().clear();
                    edit.clearComposingText();
                    edit.setText(s);
                    edit.setInputType(originalInputType);
                    cursor.applyTo(scroll, edit);
                    latestSaved = s;
                    history = new UndoRedoHelper(edit);
                    contentIsPopulated = true;
                    if (!Keyboard.isVisible(getActivity())) {
                        FAB.setVisisble(getActivity());
                    }
                    enablePreviewUpdate();
                    ProgressBar.hide(getActivity());
                } else {
                    ProgressBar.hide(getActivity());
                    onDocLoadFailure();
                }
            }

            @Override
            protected void onTimeout() {
                ProgressBar.hide(getActivity());
                onDocLoadFailure();
            }
        }.execute();
    }

    private void onDocLoadFailure() {
        if (getActivity() == null) return;
        new AlertDialog.Builder(getActivity())
                .setTitle("Failed to load document")
                .setMessage("Could not retrieve document.  Please ensure that you " +
                        "are connected to the internet and try again.")
                .setPositiveButton("Back", WelcomeFragment.showFromDialog(getActivity()))
                .show();
    }

    /**
     * @return true if save successful or nothing to save, false otherwise
     */
    private boolean save() {
        if (!contentIsPopulated) {
            // editText hasn't loaded yet, nothing to save
            return true;
        }
        String current = edit.getText().toString();
        ScrollPosition newPosition = ScrollPosition.from(scroll, edit);
        if (current.equals(latestSaved) && cursor.equals(newPosition)) {
            // nothing to save
            return true;
        }
        Optional<ContentResolver> c = Activities.contentResolver(getActivity());
        if (c.isPresent()) {
            content.write(c.get(), current);
            String name = content.filename(c.get());
            Preference.put(getActivity(), name, newPosition);
            latestSaved = current;
            return true;
        } else {
            Log.w("grok", "Missing ContentResolver.  Failed to save.");
        }
        return false;
    }

    public void enablePeriodicSave() {
        final int period_ms = Preference.AUTO_SAVE_INTERVAL_SECONDS.getInt(getActivity(), 30) * 1000;
        final int timeout_ms = period_ms / 2 >= 5000 ? period_ms / 2 : period_ms;
        periodicSave = new PeriodicAsyncTask<Void>() {
            @Override
            protected AsyncTask<Void, ?, ?> delegate() {
                return new AsyncTaskWithTimeout<Void, Void, Void>(
                        getActivity(), timeout_ms, TimeUnit.MILLISECONDS) {
                    @Override
                    protected Void runInBackground(Void... params) {
                        save();
                        return null;
                    }

                    @Override
                    protected void onTimeout() {
                        if (contentIsPopulated) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Failed to save document")
                                    .setMessage("Please ensure that you are connected to the " +
                                            "internet.  If you see this message multiple times " +
                                            "and have a proper connection, please make a " +
                                            "backup of your work and report a bug.")
                                    .show();
                        }
                    }
                };
            }
        }.schedule(period_ms, period_ms);
    }

    private void enablePreviewUpdate() {
        periodicPreview = PeriodicAsyncTask.from(() -> updatePreview()).schedule(0, 1000);
    }

    private void updatePreview() {
        if (isDualPane()) {
            String md = edit.getText().toString();
            if (!latestPreview.equals(md)) {
                PreviewFragment p = (PreviewFragment) getActivity().getFragmentManager().findFragmentById(R.id.rightContainer);
                if (p != null) {
                    p.updateContents(md);
                } else {
                    PreviewFragment f = PreviewFragment.create(md, content, ScrollPosition.from(scroll, edit));
                    getActivity().getFragmentManager().beginTransaction().replace(R.id.rightContainer, f).commit();
                }
                latestPreview = md;
            }
        }
    }
}
