package com.grok.notes;

import android.app.Activity;
import android.view.View;

public class ProgressBar {
    public static void show(final Activity a) {
        if (a != null) {
            a.runOnUiThread(() -> a.findViewById(R.id.progressBar).setVisibility(View.VISIBLE));
        }
    }

    public static void hide(final Activity a) {
        if (a != null) {
            a.runOnUiThread(() -> a.findViewById(R.id.progressBar).setVisibility(View.GONE));
        }
    }
}
