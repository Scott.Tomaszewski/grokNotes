package com.grok.notes.document;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.google.common.base.Optional;
import com.google.common.net.MediaType;
import com.grok.notes.Blank;
import com.grok.notes.Preference;
import com.grok.notes.R;
import com.grok.notes.WelcomeFragment;
import com.grok.notes.edit.EditFragment;
import com.grok.notes.preview.PreviewFragment;
import com.grok.notes.util.ScrollPosition;

import java.io.IOException;

import static com.grok.notes.util.Activities.contentResolver;

public class StorageAccessFramework {
    private static final int OPEN = 10;
    private static final int CREATE = 11;
    private static final int MOVE = 12;

    public static void openMarkdownDocument(Activity context) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        MediaType type = Preference.SAF_OPENS_ANY_TYPE.getBoolean(context, false)
                ? MediaType.ANY_TYPE
                : MediaType.ANY_TEXT_TYPE;
        intent.setTypeAndNormalize(type.toString());
        context.startActivityForResult(intent, OPEN);
    }

    public static void createNewMarkdownDocument(Activity context, String filename) {
        createDocument(context, filename, CREATE);
    }

    public static void moveMarkdownDocument(Activity context, String newFilename, String content) {
        context.getPreferences(Context.MODE_PRIVATE).edit().putString("content", content).apply();
        createDocument(context, newFilename, MOVE);
    }

    private static void createDocument(Activity context, String filename, int code) {
        Intent i = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.putExtra(Intent.EXTRA_TITLE, filename);
        i.setType(MediaType.PLAIN_TEXT_UTF_8.toString());
        context.startActivityForResult(i, code);
    }

    public static void handleResult(Activity context, int requestCode, int resultCode, Intent resultData) {
        if (resultCode == Activity.RESULT_OK) {
            if (resultData != null) {
                Uri uri = resultData.getData();
                SafDocument doc = new SafDocument(uri);
                if (requestCode == MOVE) {
                    String content = context.getPreferences(Context.MODE_PRIVATE).getString("content", "");
                    doc.write(context.getContentResolver(), content);
                }
                Backups.persist(doc, context);
                ScrollPosition cursor = getScrollPosition(context, doc);
                openDocument(context, doc, cursor);
            }
        }
    }

    private static void openDocument(Activity context, SafDocument doc, ScrollPosition cursor) {
        boolean shouldShowPreview = Preference.OPEN_PREVIEW_BY_DEFAULT.getBoolean(context, false)
                && context.findViewById(R.id.rightContainer) == null;
        try {
            Fragment toShow = shouldShowPreview
                    ? PreviewFragment.create(doc.read(contentResolver(context).get()), doc, cursor)
                    : EditFragment.of(doc, cursor);
            context.getFragmentManager().beginTransaction()
                    .replace(R.id.welcome, Blank.FRAGMENT)
                    .replace(R.id.leftContainer, toShow)
                    .commit();
        } catch (IOException e) {
            new AlertDialog.Builder(context)
                    .setTitle("Failed to load document")
                    .setMessage("Could not retrieve document.  Please ensure that you " +
                            "are connected to the internet and try again.")
                    .setPositiveButton("Back", WelcomeFragment.showFromDialog(context))
                    .show();
        }
    }

    public static boolean canHandle(int requestCode) {
        return requestCode == OPEN || requestCode == CREATE || requestCode == MOVE;
    }

    private static ScrollPosition getScrollPosition(Activity context, SafDocument doc) {
        String name = doc.filename(context.getContentResolver());
        Optional<ScrollPosition> maybe = Preference.get(context, name, ScrollPosition.class);
        return maybe.isPresent() ? maybe.get() : ScrollPosition.top();
    }
}
