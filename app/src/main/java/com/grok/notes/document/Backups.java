package com.grok.notes.document;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.res.Resources;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.common.io.FileWriteMode;
import com.google.common.io.Files;
import com.grok.notes.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Backups {
    public static void persist(SafDocument toSave, Activity context) {
        requestWriteExternalPermission(context);
        if (!externalStorageWritable()) {
            Log.w("Grok", "Failed to backup file because external storage is unavailable.");
            return;
        }
        ContentResolver cr = context.getContentResolver();
        String timestamp = SimpleDateFormat.getDateTimeInstance().format(new Date());
        String backupName = timestamp + "-" + toSave.filename(cr);
        File backup = new File(getPublicStorageDirForBackups(context.getResources()), backupName);
        try {
            toSave.source(cr).copyTo(Files.asByteSink(backup, FileWriteMode.APPEND));
            Log.i("Grok", "Backup created at: " + backupName);
        } catch (IOException e) {
            Log.e("Grok", "Failed to backup " + toSave.filename(cr) + ".  Cant write to output file.");
        }
    }

    private static boolean externalStorageWritable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    public static File getPublicStorageDirForBackups(Resources context) {
        File file = new File(Environment.getExternalStorageDirectory(),
                context.getString(R.string.app_name) + "-backups");
        if (!file.mkdirs()) {
            Log.e("Grok", "Failed to create backups directory");
        }
        return file;
    }

    private static void requestWriteExternalPermission(Activity context) {
        ActivityCompat.requestPermissions(context,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                10);
    }
}
