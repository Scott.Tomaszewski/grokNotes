package com.grok.notes.document;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.OpenableColumns;

import com.google.common.base.Charsets;
import com.google.common.io.ByteSink;
import com.google.common.io.ByteSource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SafDocument implements Parcelable {
    private final Uri document;

    public SafDocument(Uri document) {
        this.document = document;
    }

    protected SafDocument(Parcel in) {
        document = in.readParcelable(SafDocument.class.getClassLoader());
    }

    public String filename(ContentResolver context) {
        try (Cursor cursor = context.query(document, null, null, null, null, null)) {
            if (cursor != null && cursor.moveToFirst()) {
                return cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
            }
        }
        String path = document.getPath();
        int cut = path.lastIndexOf('/');
        return cut != -1 ? path.substring(cut + 1) : path;
    }

    public void move(Activity c) throws IOException {
        StorageAccessFramework.moveMarkdownDocument(c, filename(c.getContentResolver()), read(c.getContentResolver()));
    }

    public ByteSource source(final ContentResolver reader) {
        return new ByteSource() {
            @Override
            public InputStream openStream() throws IOException {
                return reader.openInputStream(document);
            }
        };
    }

    public String read(ContentResolver reader) throws IOException {
        return source(reader).asCharSource(Charsets.UTF_8).read();
    }

    public ByteSink sink(final ContentResolver destination) {
        return new ByteSink() {
            @Override
            public OutputStream openStream() throws IOException {
                return destination.openOutputStream(document);
            }
        };
    }

    public void write(final ContentResolver writer, String newContent) {
        try {
            sink(writer).asCharSink(Charsets.UTF_8).write(newContent);
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot write to " + document, e);
        }
    }

    public static final Creator<SafDocument> CREATOR = new Creator<SafDocument>() {
        @Override
        public SafDocument createFromParcel(Parcel in) {
            return new SafDocument(in);
        }

        @Override
        public SafDocument[] newArray(int size) {
            return new SafDocument[size];
        }
    };

    @Override
    public int describeContents() {
        return document.describeContents();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(document, flags);
    }
}
