package com.grok.notes;

import android.app.Application;
import android.content.SharedPreferences;

public final class GrokNotes extends Application {
    private static GrokNotes INSTANCE;
    private SharedPreferences preferences;

    public static GrokNotes getInstance() {
        return INSTANCE;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = this.getApplicationContext().getSharedPreferences("pref_key", MODE_PRIVATE);
        preferences.edit().putBoolean("is_first_run", true).commit();
        INSTANCE = this;
    }

    public boolean isFirstRun() {
        return preferences.getBoolean("is_first_run", true);
    }

    public void setRunned() {
        preferences.edit().putBoolean("is_first_run", false).commit();
    }
}
