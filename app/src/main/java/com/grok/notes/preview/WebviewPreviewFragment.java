package com.grok.notes.preview;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.Toast;

import com.grok.notes.HomeButton;
import com.grok.notes.ProgressBar;
import com.grok.notes.R;

public abstract class WebviewPreviewFragment extends PreviewFragment {
    protected WebView preview;
    private Menu m;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        this.m = menu;
    }

    @Override
    public View onCreateView(LayoutInflater i, ViewGroup container, Bundle savedInstanceState) {
        ProgressBar.show(getActivity());
        View root = i.inflate(R.layout.fragment_webview_preview, container, false);
        preview = (WebView) root.findViewById(R.id.previewContent);
        preview.getSettings().setBuiltInZoomControls(true);
        preview.getSettings().setDisplayZoomControls(false);
        preview.setWebViewClient(new WebViewClient() {
            // Scroll left on page load
            public void onPageFinished(WebView view, String url) {
                view.scrollTo(0, view.getScrollY());
            }

            // opening links delegates to intent
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return shouldOverrideUrlLoading(view, request.getUrl().toString());
            }
        });
        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cursor.applyTo(preview);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.find) {
            find();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void find() {
        View v = getActivity().getLayoutInflater().inflate(R.layout.alert_edittext, null);
        final EditText input = (EditText) v.findViewById(R.id.alert_edit);
        input.setHint("Find...");
        new AlertDialog.Builder(this.getActivity())
                .setTitle("Find in document")
                .setView(v)
                .setPositiveButton("Find", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText() != null) {
                            String text = input.getText().toString();
                            if (!text.isEmpty()) {
                                preview.setFindListener(onFindResults(text));
                                preview.findAllAsync(text);
                            }
                        }
                    }
                })
                .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel()).show();
    }

    private WebView.FindListener onFindResults(final String findQuery) {
        return new WebView.FindListener() {
            @Override
            public void onFindResultReceived(
                    int activeMatchOrdinal,
                    int numberOfMatches,
                    boolean isDoneCounting) {
                if (numberOfMatches == 0) {
                    String msg = "Could not find '" + findQuery + "' in document.";
                    Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
                } else {
                    configureFindNext(R.id.findNext);
                    configureFindNext(R.id.findPrevious);
                    m.findItem(R.id.find).setVisible(false);
                    HomeButton.of(getActivity()).setAsUpArrow(new HomeButton.Listener() {
                        @Override
                        public void onHomeButtonPressed(HomeButton pressed) {
                            preview.clearMatches();
                            pressed.setAsDrawer();
                            m.findItem(R.id.find).setVisible(true);
                            m.findItem(R.id.findNext).setVisible(false).setOnMenuItemClickListener(doNothing());
                            m.findItem(R.id.findPrevious).setVisible(false).setOnMenuItemClickListener(doNothing());
                        }
                    });
                }
            }
        };
    }

    @NonNull
    private MenuItem.OnMenuItemClickListener doNothing() {
        return item -> false;
    }

    private void configureFindNext(final int itemId) {
        MenuItem i = m.findItem(itemId);
        i.setVisible(true);
        i.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                preview.findNext(itemId == R.id.findNext);
                return true;
            }
        });
    }
}
