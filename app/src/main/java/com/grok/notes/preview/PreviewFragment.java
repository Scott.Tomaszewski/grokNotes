package com.grok.notes.preview;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.grok.notes.ProgressBar;
import com.grok.notes.R;
import com.grok.notes.document.SafDocument;
import com.grok.notes.edit.EditFragment;
import com.grok.notes.util.FAB;
import com.grok.notes.util.ScrollPosition;

public abstract class PreviewFragment extends Fragment {
    private static final String ARG_TEXT = "originalContent";
    private static final String ARG_ORIGINAL = "originalDoc";
    private static final String ARG_CURSOR = "cursor";

    protected String originalContent;
    protected SafDocument original;
    protected ScrollPosition cursor;

    public static PreviewFragment create(String toPreview, SafDocument original, ScrollPosition cursor) {
        PreviewCommonmarkFragment toShow = new PreviewCommonmarkFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, toPreview);
        args.putParcelable(ARG_ORIGINAL, original);
        args.putParcelable(ARG_CURSOR, cursor);
        toShow.setArguments(args);
        return toShow;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            originalContent = getArguments().getString(ARG_TEXT);
            original = getArguments().getParcelable(ARG_ORIGINAL);
            cursor = getArguments().getParcelable(ARG_CURSOR);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.preview, menu);
        if (!isDualPane()) {
            getActivity().setTitle(original.filename(getActivity().getContentResolver()));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                backToEdit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateContents(originalContent);
        if (!isDualPane()) {
            FloatingActionButton fab = FAB.setVisisble(getActivity()).get();
            fab.setOnClickListener(v -> backToEdit());
            FAB.setImage(getActivity(), R.drawable.ic_mode_edit_white_48dp);
        }
        ProgressBar.hide(getActivity());
    }

    private void backToEdit() {
        if (isDualPane()) {
            // TODO - this is a hack
            ((EditFragment) getFragmentManager().findFragmentById(R.id.leftContainer))
                    .editorOnly(FAB.orNull(getActivity()));
        } else {
            getActivity().getFragmentManager().beginTransaction()
                    .replace(R.id.leftContainer, EditFragment.of(original, cursor))
                    .commit();
        }
    }

    protected boolean isDualPane() {
        View maybe = getActivity().findViewById(R.id.rightContainer);
        return maybe != null && maybe.getVisibility() == View.VISIBLE;
    }

    public abstract void updateContents(String md);
}
