package com.grok.notes.preview;

import android.content.res.Resources;
import android.support.annotation.ColorInt;
import android.util.Log;
import android.util.TypedValue;

import com.grok.notes.Preference;
import com.grok.notes.R;
import com.grok.notes.util.Font;

import org.commonmark.Extension;
import org.commonmark.ext.autolink.AutolinkExtension;
import org.commonmark.ext.gfm.strikethrough.StrikethroughExtension;
import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.ext.ins.InsExtension;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import java.util.Arrays;
import java.util.List;

public class PreviewCommonmarkFragment extends WebviewPreviewFragment {
    @Override
    public void updateContents(String md) {
        List<Extension> extensions = Arrays.asList(
                TablesExtension.create(),
                StrikethroughExtension.create(),
                AutolinkExtension.create(),
                InsExtension.create());
        Parser parser = Parser.builder().extensions(extensions).build();
        HtmlRenderer renderer = HtmlRenderer.builder().extensions(extensions).build();
        Node document = parser.parse(md);
        String html = injectCss(renderer.render(document), css());
        getActivity().runOnUiThread(() ->
                preview.loadDataWithBaseURL("about:blank", html, "text/html", "utf-8", "about:blank"));
    }

    private String injectCss(String html, String css) {
        return "<head><style>" + css + "</style></head>" + html;
    }

    private String css() {
        return ""
                + "html { "
                + "  margin-bottom: 100px; "
                + "  font-family: " + Font.previewPreference(getActivity()).cssName + ";"
                + "  font-size: " + fontScale() + "%;"
                + "  background: " + getAttribute(android.R.attr.windowBackground) + ";"
                + "  color: " + getAttribute(android.R.attr.textColor) + ";"
                + "}"
                + "a { color: " + getAttribute(android.R.attr.textColorLink) + "; }"
                + "table { border-collapse: collapse; }"
                + "table, th, td { border: 1px solid #212121; }"
                + "td, th { padding: 10px; }";
    }

    private int fontScale() {
        return 100 + (5 * Preference.PREVIEW_TEXT_SIZE.getInt(getActivity(), 0));
    }

    private String getAttribute(int attribute) {
        TypedValue typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(attribute, typedValue, true);
        return String.format("#%06X", (0xFFFFFF & typedValue.data));
    }
}
