package com.grok.notes;

import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.grok.notes.document.Backups;
import com.grok.notes.edit.Divider;

public class AboutFragment extends Fragment {
    private Divider previousState;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater i, ViewGroup container, Bundle savedInstanceState) {
        View v = i.inflate(R.layout.fragment_about, container, false);
        TextView version = (TextView) v.findViewById(R.id.aboutVersion);
        version.setText("Version " + getResources().getString(R.string.versionName));
        TextView backups = (TextView) v.findViewById(R.id.backupText);
        backups.setText(backups.getText() + Backups.getPublicStorageDirForBackups(getResources()).toString());
        getActivity().setTitle("About Grok Notes");
        previousState = Divider.saveState(getActivity());
        Divider.hideIfPresent(getActivity());
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeButton.of(getActivity()).setAsBackButton();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onPause() {
        previousState.restore(getActivity());
        super.onPause();
    }
}
