package com.grok.notes.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import com.grok.notes.Preference;
import com.grok.notes.R;

public class Style {
    private static final Table<String, Preference, Integer> colorAndColorPrefToStyle =
            ImmutableTable.<String, Preference, Integer>builder()
                    // primary
                    .put("Red", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryRed)
                    .put("Pink", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryPink)
                    .put("Purple", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryPurple)
                    .put("Deep purple", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryDeepPurple)
                    .put("Indigo", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryIndigo)
                    .put("Blue", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryBlue)
                    .put("Light blue", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryLightBlue)
                    .put("Cyan", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryCyan)
                    .put("Teal", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryTeal)
                    .put("Green", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryGreen)
                    .put("Light green", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryLightGreen)
                    .put("Lime", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryLime)
                    .put("Yellow", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryYellow)
                    .put("Amber", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryAmber)
                    .put("Orange", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryOrange)
                    .put("Deep orange", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryDeepOrange)
                    .put("Brown", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryBrown)
                    .put("Grey", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryGrey)
                    .put("Blue grey", Preference.LIGHT_THEME_PRIMARY_COLOR, R.style.PrimaryBlueGrey)
                    // Accent
                    .put("Red", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentRed)
                    .put("Pink", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentPink)
                    .put("Purple", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentPurple)
                    .put("Deep purple", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentDeepPurple)
                    .put("Indigo", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentIndigo)
                    .put("Blue", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentBlue)
                    .put("Light blue", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentLightBlue)
                    .put("Cyan", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentCyan)
                    .put("Teal", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentTeal)
                    .put("Green", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentGreen)
                    .put("Light green", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentLightGreen)
                    .put("Lime", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentLime)
                    .put("Yellow", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentYellow)
                    .put("Amber", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentAmber)
                    .put("Orange", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentOrange)
                    .put("Deep orange", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentDeepOrange)
                    .put("Brown", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentBrown)
                    .put("Grey", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentGrey)
                    .put("Blue grey", Preference.LIGHT_THEME_ACCENT_COLOR, R.style.AccentBlueGrey)
                    .build();

    public static void showRestartDialog(Activity context) {
        new AlertDialog.Builder(context)
                .setTitle("Restart required")
                .setMessage("GrokNotes needs to restart in order to change the theme.  Would " +
                        "you like to restart now or apply changes on next restart? ")
                .setPositiveButton("Restart now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = context.getIntent();
                        context.finish();
                        intent.putExtra("openToWelcome", true);
                        context.startActivity(intent);
                    }
                })
                .setNegativeButton("Apply later", (dialog, which) -> dialog.cancel())
                .show();
    }

    public static void applyTo(Activity context) {
        context.setTheme(theme(context));
        applyStyles(context);
    }

    private static int theme(Activity context) {
        return isDarkTheme(context) ? R.style.AppTheme_Dark : R.style.AppTheme;
    }

    private static void applyStyles(Activity context) {
        if (isDarkTheme(context)) {
            return;
        }
        context.getTheme().applyStyle(getStyleFor(context, Preference.LIGHT_THEME_PRIMARY_COLOR), true);
        context.getTheme().applyStyle(getStyleFor(context, Preference.LIGHT_THEME_ACCENT_COLOR), true);
    }

    private static int getStyleFor(Activity context, Preference colorPref) {
        String primary = colorPref.getString(context, "Teal");
        return colorAndColorPrefToStyle.get(primary, colorPref);
    }

    private static boolean isDarkTheme(Activity context) {
        return Preference.THEME.getString(context, "light").equals("dark");
    }
}
