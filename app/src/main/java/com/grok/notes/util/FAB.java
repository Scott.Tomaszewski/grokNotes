package com.grok.notes.util;

import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.google.common.base.Optional;
import com.grok.notes.R;


public class FAB {
    public static FloatingActionButton orNull(Activity a) {
        return (FloatingActionButton) a.findViewById(R.id.fab);
    }

    public static Optional<FloatingActionButton> get(Activity a) {
        return Optional.fromNullable((FloatingActionButton) a.findViewById(R.id.fab));
    }

    public static Optional<FloatingActionButton> setVisisble(Activity a) {
        return setFab(a, View.VISIBLE);
    }

    public static Optional<FloatingActionButton> setGone(Activity a) {
        return setFab(a, View.GONE);
    }

    private static Optional<FloatingActionButton> setFab(Activity a, int visibility) {
        Optional<FloatingActionButton> maybe = get(a);
        if (maybe.isPresent()) {
            maybe.get().setVisibility(visibility);
        }
        return maybe;
    }

    public static Optional<FloatingActionButton> setImage(Activity a, int drawable) {
        Optional<FloatingActionButton> maybe = get(a);
        if (maybe.isPresent()) {
            maybe.get().setImageDrawable(a.getDrawable(drawable));
        }
        return maybe;
    }

    public static Optional<FloatingActionButton> doNothingOnClick(Activity a) {
        Optional<FloatingActionButton> maybe = get(a);
        if (maybe.isPresent()) {
            maybe.get().setOnClickListener(v -> {
            });
        }
        return maybe;
    }

    private FAB() {
    }
}
