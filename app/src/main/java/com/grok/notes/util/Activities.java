package com.grok.notes.util;

import android.app.Activity;
import android.content.ContentResolver;

import com.google.common.base.Optional;

public final class Activities {
    public static Optional<ContentResolver> contentResolver(Activity context) {
        if (context != null) {
            ContentResolver c = context.getContentResolver();
            if (c != null) {
                return Optional.of(c);
            }
        }
        return Optional.absent();
    }

    private Activities() {
    }
}
