package com.grok.notes.util;

import android.os.AsyncTask;
import android.os.Handler;

import java.util.Timer;
import java.util.TimerTask;

public abstract class PeriodicAsyncTask<Params> {
    /**
     * @param runInBackground must be able to be run multiple times
     */
    public static <Void> PeriodicAsyncTask<Void> from(final Runnable runInBackground) {
        return new PeriodicAsyncTask<Void>() {
            @Override
            protected AsyncTask<Void, Void, Void> delegate() {
                return new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        runInBackground.run();
                        return null;
                    }
                };
            }
        };
    }

    /**
     * This MUST return a new instance of an {@link AsyncTask} on every execution since
     * {@link AsyncTask} can only be run once.
     */
    protected abstract AsyncTask<Params, ?, ?> delegate();

    /**
     * @param delay  in ms
     * @param period in ms
     */
    public Timer schedule(long delay, long period, final Params... args) {
        final Handler handler = new Handler();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(() -> delegate().execute(args));
            }
        };
        Timer periodicSave = new Timer();
        periodicSave.schedule(doAsynchronousTask, delay, period);
        return periodicSave;
    }
}
