package com.grok.notes.util;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.grok.notes.Preference;

public enum Font {
    SYSTEM("sans-serif", Typeface.DEFAULT),
    SANS_SERIF("sans-serif", Typeface.SANS_SERIF),
    SERIF("serif", Typeface.SERIF),
    MONOSPACE("monospace", Typeface.MONOSPACE);

    public static Font editorPreference(Context c) {
        return Font.forName(Preference.EDITOR_FONT.getString(c, SYSTEM.cssName), Font.SYSTEM);
    }

    public static Font previewPreference(Context c) {
        return Font.forName(Preference.PREVIEW_FONT.getString(c, SYSTEM.cssName), Font.SYSTEM);
    }

    private static final Font forName(String name, Font secondChoice) {
        for (Font typeface : values()) {
            if (typeface.cssName.equals(name)) {
                return typeface;
            }
        }
        return secondChoice;
    }

    public final String cssName;
    public final Typeface actual;

    Font(String cssName, Typeface actual) {
        this.cssName = cssName;
        this.actual = actual;
    }

    public void applyTo(ViewGroup toApplyTo) {
        replaceTypeface(toApplyTo, actual);
    }

    // https://coderwall.com/p/qxxmaa/android-use-a-custom-font-everywhere
    private static void replaceTypeface(ViewGroup viewTree, Typeface font) {
        for (int i = 0; i < viewTree.getChildCount(); ++i) {
            View child = viewTree.getChildAt(i);
            if (child instanceof ViewGroup) {
                // recursive call
                replaceTypeface((ViewGroup) child, font);
            } else if (child instanceof TextView) {
                // base case
                ((TextView) child).setTypeface(font);
            }
        }
    }
}
