package com.grok.notes.bible;

import android.util.Log;

import com.google.common.base.Optional;

import java.util.Arrays;
import java.util.List;

public class Passage {
    private final String contents;
    private final BookChapterVerse reference;
    private final String link;

    public static Optional<Passage> parse(String unknownText) {
        if (unknownText.contains("bible.com")) {
            Optional<Passage> maybe = fromYouVersion(unknownText);
            return maybe.isPresent() ? maybe : fromBibleSite(unknownText);
        }
        return Optional.absent();
    }

    private static Optional<Passage> fromBibleSite(String text) {
        List<String> words = Arrays.asList(text.split("\\s"));
        for (String maybe : words) {
            if (maybe.contains(".bible.com")) {
                String url = maybe;
                String content = text.replace(url, "");
                Optional<BookChapterVerse> bcv = BookChapterVerse.fromBibleSiteUrl(url);
                return bcv.isPresent()
                        ? Optional.of(new Passage(content, bcv.get(), url))
                        : Optional.<Passage>absent();
            }
        }
        Log.w("grok", "Found 'bible.com', but couldn't extract url: " + text);
        return Optional.absent();
    }

    private static Optional<Passage> fromYouVersion(String copiedText) {
        try {
            List<String> lines = Arrays.asList(copiedText.split("\\r?\\n"));
            String link = lines.get(lines.size() - 1);
            String ref = lines.get(lines.size() - 2);
            String content = copiedText.substring(0, copiedText.indexOf(ref));
            Optional<BookChapterVerse> bcv = BookChapterVerse.parse(ref);
            return bcv.isPresent()
                    ? Optional.of(new Passage(content, bcv.get(), link))
                    : Optional.<Passage>absent();
        } catch (IndexOutOfBoundsException e) {
            Log.w("grok", "Tried parsing assuming YouVersion passage, but failed. " + copiedText);
            return Optional.absent();
        }
    }

    private Passage(String contents, BookChapterVerse reference, String link) {
        this.contents = contents;
        this.reference = reference;
        this.link = link;
    }

    public String asMarkdown() {
        return String.format("> %s\n> <sub>[%s](%s)</sub>", contents.trim(), reference, link.trim());
    }
}
