package com.grok.notes;

import com.google.common.collect.Lists;

import org.junit.Test;

import java.util.Arrays;

import static com.grok.notes.FormattingBarItem.lineIndexBeforeSelection;
import static com.grok.notes.FormattingBarItem.lineIndicesWithinSelection;
import static org.junit.Assert.assertEquals;

public class FormattingBarItemTest {
    @Test
    public void findLineIndicesForSelection() throws Exception {
        assertEquals(
                Arrays.asList(0),
                FormattingBarItem.findLineIndicesForSelection("", 0, 0, false));
        assertEquals(
                Arrays.asList(0),
                FormattingBarItem.findLineIndicesForSelection("\n", 0, 0, false));
        assertEquals(
                Arrays.asList(1),
                FormattingBarItem.findLineIndicesForSelection("\n", 1, 1, false));
        assertEquals(
                Arrays.asList(0),
                FormattingBarItem.findLineIndicesForSelection("the", 1, 1, false));
        assertEquals(
                Arrays.asList(0),
                FormattingBarItem.findLineIndicesForSelection("a\n", 1, 1, false));
        assertEquals(
                Arrays.asList(2),
                FormattingBarItem.findLineIndicesForSelection("a\n", 2, 2, false));
        assertEquals(
                Arrays.asList(0, 2),
                FormattingBarItem.findLineIndicesForSelection("a\n", 0, 2, false));
        assertEquals(
                Arrays.asList(0, 2),
                FormattingBarItem.findLineIndicesForSelection("a\n\n", 0, 2, false));
        assertEquals(
                Arrays.asList(1),
                FormattingBarItem.findLineIndicesForSelection("\na", 2, 2, false));
        assertEquals(
                Arrays.asList(1),
                FormattingBarItem.findLineIndicesForSelection("\na", 1, 1, false));
        assertEquals(
                Arrays.asList(0),
                FormattingBarItem.findLineIndicesForSelection("\na", 0, 0, false));
        assertEquals(
                Arrays.asList(0, 2, 3),
                FormattingBarItem.findLineIndicesForSelection("a\n\na", 0, 4, false));
    }

    @Test
    public void lineIndexesBeforeSelection() {
        assertEquals(0, lineIndexBeforeSelection("", 0, false));
        assertEquals(1, lineIndexBeforeSelection("\na", 1, false));
        assertEquals(1, lineIndexBeforeSelection("\na", 2, false));
        assertEquals(0, lineIndexBeforeSelection("\n", 0, false));
        assertEquals(1, lineIndexBeforeSelection("\n", 1, false));
        assertEquals(1, lineIndexBeforeSelection("\na\n", 2, false));
        assertEquals(3, lineIndexBeforeSelection("\na\n", 3, false));

        assertEquals(1, lineIndexBeforeSelection(" ", 1, true));
        assertEquals(2, lineIndexBeforeSelection("\n a", 2, true));
        assertEquals(2, lineIndexBeforeSelection("\n a", 3, true));
        assertEquals(1, lineIndexBeforeSelection(" \n", 1, true));
        assertEquals(2, lineIndexBeforeSelection("\n ", 2, true));
        assertEquals(2, lineIndexBeforeSelection("\n a\n", 3, true));
        assertEquals(4, lineIndexBeforeSelection("\na\n ", 4, true));
        // TODO - \r\n
    }

    @Test
    public void lineIndicesWithinSelectionTest() {
        assertEquals(Lists.newArrayList(), lineIndicesWithinSelection("", 0, 0, false));
        assertEquals(Lists.newArrayList(), lineIndicesWithinSelection("   ", 0, 3, false));
        assertEquals(Lists.newArrayList(), lineIndicesWithinSelection("a", 0, 1, false));
        assertEquals(Lists.newArrayList(), lineIndicesWithinSelection("\n", 0, 0, false));
        assertEquals(Lists.newArrayList(1), lineIndicesWithinSelection("\n", 0, 1, false));
        assertEquals(Lists.newArrayList(2), lineIndicesWithinSelection("a\n", 0, 2, false));
        assertEquals(Lists.newArrayList(1), lineIndicesWithinSelection("\na", 0, 2, false));
        assertEquals(Lists.newArrayList(1, 3), lineIndicesWithinSelection("\na\n", 0, 3, false));

        assertEquals(Lists.newArrayList(), lineIndicesWithinSelection("", 0, 0, true));
        assertEquals(Lists.newArrayList(), lineIndicesWithinSelection("   ", 0, 3, true));
        assertEquals(Lists.newArrayList(), lineIndicesWithinSelection("   a", 4, 4, true));
        assertEquals(Lists.newArrayList(5), lineIndicesWithinSelection("\n    a", 0, 6, true));
        assertEquals(Lists.newArrayList(2), lineIndicesWithinSelection("\n ", 0, 1, true));
        assertEquals(Lists.newArrayList(2), lineIndicesWithinSelection("\n ", 0, 2, true));
        assertEquals(Lists.newArrayList(3), lineIndicesWithinSelection("a \n", 0, 3, true));
        assertEquals(Lists.newArrayList(2), lineIndicesWithinSelection("\n a", 0, 3, true));
        assertEquals(Lists.newArrayList(2, 5), lineIndicesWithinSelection("\n a\n ", 0, 5, true));
    }
}